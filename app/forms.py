'''Forms document'''
from flask.ext.wtf import TextAreaField, Length, SelectField, URL, Optional
from flask.ext.wtf import Form, TextField, BooleanField, Required, Regexp
from config import HTTP_REQUESTS
from wtforms import validators, PasswordField, FormField, FieldList, SubmitField
from models import User, Device, Room, Room_User, Shared_Device
from flask import g

class LoginForm(Form):
    '''Login form'''
    openid = TextField('openid', validators=[Required()])
    remember_me = BooleanField('remember_me', default=False)

#registration
class RegistrationForm(Form):
    '''Registration form'''
    first_name = TextField('First Name *', [validators.Required()])
    last_name = TextField('Last Name *', [validators.Required()])
    email = TextField('Email Address *', [validators.Required()])
    nickname = TextField('Username *', [validators.Required()])
    password = PasswordField('Password *', [validators.Required(), 
              validators.EqualTo('confirm_password', 
              message='Passwords must match')])
    confirm_password = PasswordField('Confirm Password *', 
        [validators.Required()])

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.user = None

    def validate(self):
        '''Custom validator'''
        errors = 0
        rv = Form.validate(self)
        if not rv:
            return False

        #checking if nickname is taken
        user = User.query.filter_by(nickname = self.nickname.data).first()
        if user is not None:
            nickname_error = ('Nickname is already taken. '+
	            'Please choose another nickname!')
            self.nickname.errors.append(nickname_error)
            errors = errors + 1

        #checking if email address is taken
        user = User.query.filter_by(email = self.email.data).first()
        if user is not None:
            self.email.errors.append('Email Address already exists!')
            errors = errors + 1

        if(errors != 0):
            return False

        return True

#login using smart house data
class LoginForm2(Form):
    '''Login Form 2'''
    email = TextField('Email Address *', [validators.Required()])
    password = PasswordField('Password *', [validators.Required()])

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.user = None

    def validate(self):
        '''custom validation'''
        errors = 0
        rv = Form.validate(self)
        if not rv:
            return False

        user = User.query.filter_by(
            email = self.email.data).first()
        if user is None:
            errors = errors + 1 

        if(errors == 0):
            if not user.check_password(self.password.data):
                errors = errors + 1

        if(errors != 0):
            self.email.errors.append('Email Address and/or'+
                ' password is invalid!')
            self.password.errors.append('Email Address and/or'+
                ' password is invalid!')
            return False

        self.user = user
        return True
	
class EditForm(Form):
    '''User profile edit form'''
    nickname = TextField('nickname *', validators = [Required(),
               Regexp('^[\S]*$')])
    email = TextField('Email Address *', [validators.Required()])
    first_name = TextField('First Name *', [validators.Required()])
    last_name = TextField('Last Name *', [validators.Required()])
    about_me = TextAreaField('about_me', validators = [Length(min=0, max=140)])
    nickname = TextField('Username *', [validators.Required()])

	
    def __init__(self, original_nickname, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.original_nickname = original_nickname
    def validate(self):
        '''custom validation'''
        if not Form.validate(self):
            return False
        if self.nickname.data == self.original_nickname:
            return True
        user = User.query.filter_by(nickname = self.nickname.data).first()
        if user != None:
            self.nickname.errors.append("""This nickname is already 
            in use. Please choose another one.""")
            return False
        return True

class EditPasswordForm(Form):
    '''Password change form'''
    old_password = PasswordField('Current Password *', [validators.Required()])
    new_password = PasswordField('New Password *', [validators.Required(), 
                   validators.EqualTo('confirm_password', 
                   message='Passwords must match')])
    confirm_password = PasswordField('Confirm Password', 
                       [validators.Required()])

	
    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.user = None

    def validate(self):
        '''custom validation'''
        rv = Form.validate(self)
        if not rv:
            return False
        user = User.query.filter_by(nickname=g.user.nickname, 
               password = self.old_password.data).first()
        if user is None:
            self.old_password.errors.append('Incorrect password!')
            return False
        return True

class PostForm(Form):
    '''Post form'''
    post = TextField('post', validators = [Required()])

class RoomForm(Form):
    '''Room Form'''
    enable = BooleanField('enable', default=True)
    name = TextField('name *', validators = [Required(), Regexp('^[\S]*$')])
    webcam_url = (TextField('webcam url', validators = [Optional(), 
        URL(message="Invalid url.")]))

    def __init__(self, original_name, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.original_name = original_name.lower()
		
    def validate(self):
        '''custom validation'''
        if not Form.validate(self):
            return False
        if self.name.data.lower() == self.original_name.lower():
            return True
        room = (Room.query.filter_by(name = self.name.data.lower(), 
            user_id = g.user.id).first())
        if room != None:
            self.name.errors.append("""This room name is already in use. 
            Please choose another one.""")
            return False
        shared_room = Room_User.query.filter_by(room_name = 
                      self.name.data.lower(), user_id = g.user.id).first()
        if shared_room != None:
            self.name.errors.append("""This room name is already in use. 
            Please choose another one.""")
            return False
        return True
        	
class DeviceForm(Form):
    '''Device forms'''
    enable = BooleanField('enable', default=True)
    name = TextField('name *', validators = [Required(), Regexp('^[\S]*$')])
    request_type = SelectField('request_type *', choices = HTTP_REQUESTS, 
                   validators = [Required()])
    command = TextField('command', validators = [ Regexp('^[\S]*$'), 
              Length(max=16)]) # might need to be more robust
    command_optional = TextField('command_optional', 
                       validators = [Regexp('^[\S]*$'),
                       Length(max=16)]) # might need to be more robust
    room = TextField('room', validators = [Required()], default='House')
    # make sure url starts with http:// or https://
    url = TextField('url', validators = [Required(), Length(min=8, max=256),
          URL()])
	
    def __init__(self, original_name, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.original_name = original_name.lower()
		
    def validate(self):
        '''custom validation'''
        if not Form.validate(self):
            return False
        if self.name.data.lower() == self.original_name.lower():
            return True
        room = (Room.query.filter_by(name = self.room.data.lower(), 
            user = g.user).first())
        if room:
            if self.name.data.lower() in room.list_devices():
                self.name.errors.append('This device name already exists in'+
                    ' room '+self.room.data+'. Please choose another one.')
                return False
        if (self.command.data.lower() == 
            self.command_optional.data.lower() and len(self.command.data) >0):
            self.command_optional.errors.append("""This command already 
                                                exists for this 
                                                device. Choose another one.""")
            return False
        return True

class AddAdminForm(Form):
    '''form to add admins'''
    email = TextField('Enter Email Address *', [validators.Required()])
    confirm_email = TextField('Confirm Email Address *', [validators.Required(), 
                    validators.EqualTo('email', message='Emails must match')])

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.user = None

    def validate(self):
        '''custom validation'''
        errors = 0
        rv = Form.validate(self)
        if not rv:
            return False

        #checking if the email address is registered on smart house web app
        user = User.query.filter_by(email = self.email.data).first()
        if user is None:
	    admin_error = ('The email address owner is not a member of Smart House.')
            self.email.errors.append(admin_error)
            return False

        return True

class SharedDeviceForm(Form):
    '''form to share device'''
    email = TextField('Enter Email Address *', [validators.Required()])

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.user = None

    def validate(self):
        '''custom validation'''
        errors = 0
        rv = Form.validate(self)
        if not rv:
            return False

        #checking if the email address is registered on smart house web app
        user = User.query.filter_by(email = self.email.data).first()
        if user is None:
            self.email.errors.append("""The email address owner is not 
                                     a member of Smart House.""")
            return False

        #checking if the email address is already linked to the room

        shared_device = (Shared_Device.query.filter_by(user_id = user.id, 
            			     device_id = g.device_id).first())

        if shared_device is not None:
            self.email.errors.append("""The email address owner already 
                                     has access to the device.""")
            return False
        return True

class AddUsersForm(Form):
    '''form to add users'''
    email = TextField('Enter Email Address *', [validators.Required()])
    privilege = SelectField('Privilege', choices = [('1', 'Read/Write'), 
                ('0', 'Read')])

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
        self.user = None

    def validate(self):
        '''custom validation'''

        if not Form.validate(self):

	    if(len(self.email.errors) == 1): #checking validators.Required 
                return False

            #checking if the email address is registered 
            #on smart house web app
            user = User.query.filter_by(email = self.email.data).first()

            if user is None:
                self.email.errors.append("""The email address owner is not 
                                         a member of Smart House.""")
                return False

            #checking if the email address is already linked to the room
            room_user = (Room_User.query.filter_by(user_id = user.id, 
                			room_id = g.room_id).first())

        if room_user is not None:
            self.email.errors.append("""The email address owner is already 
					 part of the room.""")
            return False
        return True

class AddRoomUsersForm(Form):
    '''form to add users to room'''
    users = FieldList(FormField(AddUsersForm), min_entries = 1)
    add_another_user = SubmitField()

class CreateForumForm(Form):
    '''form to create a forum post'''
    title = TextField('Title *', [validators.Required()])
    body = TextAreaField('Body *', [validators.Required()])

class ForumForm(Form):
    '''form to reply to a forum post'''
    body = TextAreaField('Body *', [validators.Required()])

class CreateMessageForm(Form):
    '''form to send a message'''
    title = TextField('Title *', [validators.Required()])
    body = TextAreaField('Body *', [validators.Required()])

class ReplyMessageForm(Form):
    '''form to reply to a message'''
    body = TextAreaField('Body *', [validators.Required()])

class ContactForm(Form):
    '''contact us form'''
    email = TextField('Email *', [validators.Required()])
    body = TextAreaField('Body *', [validators.Required()])
