'''Models.py'''
from app import db
from hashlib import md5

ROLE_USER = 0
ROLE_ADMIN = 1

ROLE_READ = 0
ROLE_READ_WRITE = 1

ROLE_SENDER = 0
ROLE_RECIPIENT = 1

def week_day(the_date):
    '''docstring'''
    day = the_date.weekday()
    convert_weekday = {"0": "Monday", "1": "Tuesday", "2": "Wednesday", 
        "3": "Thursday", "4": "Friday", "5": "Saturday", "6": "Sunday"}
    return convert_weekday[str(day)] 

def the_month(the_date):
    '''docstring'''
    month = the_date.month
    convert_month = {"1": "January", "2": "February", "3": "March", 
        "4": "April", "5": "May", "6": "June", "7": "July", 
        "8": "August", "9": "September", "10": "October", 
        "11": "November", "12": "December"}
    return convert_month[str(month)] 

def minute(the_date):
    '''return minutes => add 0 prefix if minute is 1 digit'''
    minutes = str(the_date.minute) 
    if len(minutes) == 1:
        return '0' + minutes
    else:
	return minutes    

class User(db.Model):
    '''docstring'''
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key = True)
    nickname = db.Column(db.String(64), unique = True)
    email = db.Column(db.String(120), unique = True)
    role = db.Column(db.SmallInteger, default = ROLE_USER)
    posts = db.relationship('Post', backref = 'author', lazy = 'dynamic')
    devices = db.relationship('Device', backref = 'user', lazy = 'dynamic')
    rooms = db.relationship('Room', backref = 'user', lazy = 'dynamic')
    log_entries = db.relationship('Log_Entry', backref = 'user', 
        lazy = 'dynamic')
    first_name = db.Column(db.String(64))
    last_name = db.Column(db.String(64))
    password = db.Column(db.String(64))	
    #password has to be hashed before it is stored
    date_registered = db.Column(db.DateTime)
    about_me = db.Column(db.String(140)) 
    last_seen = db.Column(db.DateTime)
    status = db.Column(db.String(8), default="Enabled")
    shared_devices = db.relationship('Shared_Device', backref = 'user',
        lazy = 'dynamic')
    #shared_rooms = db.relationship('Shared_Room', backref = 'user',
    #lazy = 'dynamic')
    shared_rooms = db.relationship('Room_User', backref = 'user', 
        lazy = 'dynamic')

    def avatar(self, size):
        '''Function that returns avatar url'''
    #Can be configured to use other avatars
        return str('http://gravatar.com/avatar/'+md5(self.email).hexdigest()+
            '?d=mm&s='+str(size))
	
    @staticmethod
    def make_unique_nickname(nickname):
        '''Function to create a unique name'''
        if User.query.filter_by(nickname = nickname).first() == None:
            return nickname
        version = 2
        while True:
            new_nickname = nickname + str(version)
            if User.query.filter_by(nickname = new_nickname).first() == None:
                break
            version += 1
        return new_nickname
		
    def is_authenticated(self):
        '''Function that returns true if user is initialized'''
        return True

    def is_active(self):
        '''Function that returns true if user is initialized'''
        return True

    def is_anonymous(self):
        '''Function that returns false if user is initialized'''
        return False

    def get_id(self):
        '''Function that returns the user id'''
        return unicode(self.id)

    def get_posts(self):
        '''Function that returns a query of all user's post'''
        return self.posts.all()

    def get_devices(self):
        '''Function that returns a query of all user's devices'''
        return self.devices.all()

    def list_rooms(self):
        '''Function that lists the names of all of user's rooms'''
        rl = []
        for r in self.rooms:
            rl.append(r.name.lower())
        return rl
	
    def list_devices(self):
        '''Function that lists the names of all of user's devices'''
        dl = []
        for d in self.devices:
            dl.append(d.name.lower())
        return dl

    def get_week_day(self):
        '''Returns Weekday'''
        return week_day(self.last_seen)

    def get_month(self):
        '''Returns Month'''
        return the_month(self.last_seen)

    def get_minute(self):
        return minute(self.last_seen) 
        
    def __repr__(self):
        '''Returns name of object'''
        return '<User %r>' % (self.nickname)
       
    #this function is used for authentication in the login process. 
    #it checks if the password the user supplied is the same as the password 
    #stored in the database
    def check_password(self, password):
        '''Function to check user's password'''
        if password == self.password:
            return True
        return False

class Log_Entry(db.Model):
    '''Log model'''
    __tablename__ = 'log_entry'
    id = db.Column(db.Integer, primary_key = True)
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def get_week_day(self):
        '''Returns Weekday'''
        return week_day(self.timestamp)

    def get_month(self):
        '''Returns Month'''
        return the_month(self.timestamp)

    def get_minute(self):
        return minute(self.timestamp) 
    
    def __repr__(self):
        '''Returns name of object'''
        return '<Entry %r>' % (self.body)

class Post(db.Model):
    '''Post model'''
    __tablename__ = 'post'
    id = db.Column(db.Integer, primary_key = True)
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def get_week_day(self):
        '''Returns Weekday'''
        return week_day(self.timestamp)

    def get_month(self):
        '''Returns Month'''
        return the_month(self.timestamp)

    def get_minute(self):
        return minute(self.timestamp) 

    def __repr__(self):
        '''Returns name of object'''
        return '<Post %r>' % (self.body)
 
class Room(db.Model):
    '''Room model'''
    __tablename__ = 'room'
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(32))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    devices = db.relationship('Device', backref = 'room', lazy = 'dynamic')
    enable = db.Column(db.Boolean(), default= True)
    shared_rooms = db.relationship('Shared_Room', backref = 'room', 
        lazy = 'dynamic')
    room_users = db.relationship('Room_User', backref = 'room', 
        lazy = 'dynamic')
    webcam_url = db.Column(db.String(256), default = "")

    @staticmethod
    def make_unique_name(name):
        '''docstring'''
        if (Room.query.filter_by(name = name, 
            user_id = self.user_id).first() == None):
            return name
        version = 2
        while True:
            new_name = name + str(version)
            if (Rooms.query.filter_by(name = new_name, 
                user_id = self.user_id).first() == None):
                break
            version += 1
        return new_name
	
    def list_devices(self):
        '''docstring'''
        dl = []
        for d in self.devices:
            dl.append(d.name.lower())
        return dl

    def is_room_shared(self):
        '''docstring'''
        shared_room = Room_User.query.filter_by(room_id = self.id).first()
        if shared_room is not None:
            return True
        else:
            return False

    def room_owner(self):
        '''docstring'''
        user = User.query.filter_by(id = self.user_id).first()
        return user
		
    def __repr__(self):
        '''Returns name of object'''
        return '<Room %r>' % (self.name)
        
class Device(db.Model):
    '''docstring'''
    __tablename__ = 'device'
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(16))
    url = db.Column(db.String(256)) 
    command = db.Column(db.String(16))
    command_optional = db.Column(db.String(16))
    #commands =  db.relationship('Command', backref = 'device',
    #lazy = 'dynamic')
    request_type = db.Column(db.String()) # from HTTP_REQUEST list
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    room_id = db.Column(db.Integer, db.ForeignKey('room.id'))
    enable = db.Column(db.Boolean(), default= True)
    message = db.Column(db.String(120))
    last_used = db.Column(db.DateTime)
    shared_devices = db.relationship('Shared_Device', backref = 'device', 
        lazy = 'dynamic')
	
    @staticmethod
    def make_unique_name(name):
        '''Returns new name'''
        if (Device.query.filter_by(name = name, 
            user_id = self.user_id).first() == None):
            return name
        version = 2
        while True:
            new_name = name + str(version)
            if (Device.query.filter_by(name = new_name, 
                user_id = self.user_id).first() == None):
                break
            version += 1
        return new_name

    def get_week_day(self):
        '''Return Weekday'''
        return week_day(self.last_used)

    def get_month(self):
        '''Return Month'''
        return the_month(self.last_used)

    def get_minute(self):
        return minute(self.last_used) 
	
    def __repr__(self):
        '''Returns name of object'''
        return '<Device %r>' % (self.name)

class Shared_Device(db.Model):
    '''Shared device model'''
    __tablename__ = 'shared_device'
    id = db.Column(db.Integer, primary_key = True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    device_id = db.Column(db.Integer, db.ForeignKey('device.id'))
    owner_id = db.Column(db.Integer)
    devices = db.relationship('Device', backref = 'device', lazy = 'dynamic')

    def __repr__(self):
        '''Returns name of object'''
        return '<Shared_Device %r>' % (self.id)

    def device_owner(self):
        '''Returns device owner'''
        device = Device.query.filter_by(id=self.device_id).first()
        user = User.query.filter_by(id=device.user_id).first()
        return user.nickname

    def get_room(self):
        '''Returns room'''
        device = Device.query.filter_by(id=self.device_id).first()
        room = Room.query.filter_by(id=device.room_id).first()
        return room

class Shared_Room(db.Model):
    '''Shared Room object'''
    __tablename__ = 'shared_room'
    id = db.Column(db.Integer, primary_key = True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    room_id = db.Column(db.Integer, db.ForeignKey('room.id'))
    owner_id = db.Column(db.Integer)

    def __repr__(self):
        '''Returns name of object'''
        return '<Shared_Room %r>' % (self.id)

    def room_owner(self):
        '''Returns room owner'''
        room = Room.query.filter_by(id=self.room_id).first()
        user = User.query.filter_by(id=room.user_id).first()
        return user.nickname

class Room_User(db.Model):
    '''User Room object'''
    __tablename__ = 'room_user'
    id = db.Column(db.Integer, primary_key = True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    room_id = db.Column(db.Integer, db.ForeignKey('room.id'))
    room_name = db.Column(db.String(32))
    privilege = db.Column(db.Integer)
    rooms = db.relationship('Room', backref = 'room', lazy = 'dynamic')

    def __repr__(self):
        '''Returns name of object'''
        return '<Room_User %r>' % (self.room_id)

    def room_user(self):
        '''Returns room user'''
        user = User.query.filter_by(id = self.user_id).first()
        return user


class Forum(db.Model):
    '''Forum topic model'''
    __tablename__ = 'forum'
    id = db.Column(db.Integer, primary_key = True)
    creator_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    title = db.Column(db.String(140))
    date_created = db.Column(db.DateTime)
    forum_posts = db.relationship('Forum_Post', backref = 'forum', 
        lazy = 'dynamic')

    def creator(self):
        '''docstring'''
        user = User.query.filter_by(id = self.creator_id).first()
        return user

    def get_forum_posts(self):
        '''docstring'''
        return self.forum_posts.all()

    def get_week_day(self):
        '''docstring'''
        return week_day(self.date_created)

    def get_month(self):
        '''docstring'''
        return the_month(self.date_created)

    def get_minute(self):
        return minute(self.date_created) 

    def __repr__(self):
        '''Returns name of object'''
        return '<Forum %r>' % (self.title)

class Forum_Post(db.Model):
    '''Forum post model'''
    __tablename__ = 'forum_post'
    id = db.Column(db.Integer, primary_key = True)
    forum_id = db.Column(db.Integer, db.ForeignKey('forum.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    body = db.Column(db.String(140))
    date_added = db.Column(db.DateTime)

    def posted_by(self):
        '''Returns who posted'''
        user = User.query.filter_by(id = self.user_id).first()
        return user

    def get_week_day(self):
        '''Returns Weekday'''
        return week_day(self.date_added)

    def get_month(self):
        '''Returns Month'''
        return the_month(self.date_added)

    def get_minute(self):
        return minute(self.date_added) 

    def __repr__(self):
        '''Returns name of object'''
        return '<Forum_Post %r>' % (self.body)


class User_Messages(db.Model):
    '''User messages model'''
    __tablename__ = 'user_messages'
    id = db.Column(db.Integer, primary_key = True)
    linked_message_id = db.Column(db.Integer) 
    user_id = db.Column(db.Integer) 
    title = db.Column(db.String(140))
    date = db.Column(db.DateTime)
    message_threads = db.relationship('Message_Thread', 
        backref = 'user_messages', lazy = 'dynamic')

    def get_message_posts(self):
        '''Returns Get_Message_posts'''
        return self.message_threads.all()

    def get_week_day(self):
        '''Return Weekday'''
        return week_day(self.date)

    def get_month(self):
        '''Return Month'''
        return the_month(self.date)

    def get_minute(self):
        return minute(self.date) 

    def __repr__(self):
        '''Returns name of object'''
        return '<Message %r>' % (self.title)


class Message_Thread(db.Model):
    '''Message thread model'''
    __tablename__ = 'message_thread'
    id = db.Column(db.Integer, primary_key = True)
    message_id = db.Column(db.Integer, db.ForeignKey('user_messages.id'))
    user_id = db.Column(db.Integer)
    other_user_id = db.Column(db.Integer)
    role = db.Column(db.Integer)
    body = db.Column(db.String)
    date = db.Column(db.DateTime)

    def user(self):
        '''Returns user'''
        user = User.query.filter_by(id = self.user_id).first()
        return user

    def other_user(self):
        '''Returns user'''
        user = User.query.filter_by(id = self.other_user_id).first()
        return user

    def get_week_day(self):
        '''Return Weekday'''
        return week_day(self.date)

    def get_minute(self):
        return minute(self.date) 

    def get_month(self):
        '''Return Month'''
        return the_month(self.date)

    def __repr__(self):
        '''Returns name of object'''
        return '<Message_Thread %r>' % (self.body)
