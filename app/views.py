'''Views.py'''
# pylint: disable=E1101
from flask import render_template, flash, redirect, session, url_for, request, g
from flask.ext.login import login_user, logout_user
from flask.ext.login import current_user, login_required
from app import app, db, lm, oid, mail
from models import * 
from forms import *
from datetime import datetime
from config import MAX_LOG_ENTRY
import requests
#from flask_mail import Message
from flask.ext.mail import Message

####################################################################
############# Home Page, My House & How To              ############
####################################################################
@app.route('/', methods =['GET', 'POST'])
@app.route('/home', methods =['GET', 'POST'])
def home():
    '''Home page'''
    # Only return the page
    return render_template("home.html", title="Home")

@app.route('/howto')
def how_to():
    '''How to use the site'''
    return render_template("how_to.html")

@app.route('/myhouse/')
@login_required
def my_house():
    '''List of rooms and devices in the house'''
    if g.user.is_authenticated():
        return render_template("my_house.html", title = g.user.nickname+
            "'s Smart House",rooms = g.user.rooms.order_by(Room.name).all(),
	    shared_devices = g.user.shared_devices.all(), 
            shared_rooms = g.user.shared_rooms.all())
    else:
        return redirect(url_for('home'))

@app.route('/myhouse_shared_rooms/')
@login_required
def my_house_shared_rooms():
    '''Shared rooms'''
    
    ''' shared_room = Room_User.query.all()

    if shared_room is not None:
        for user in shared_room:
            db.session.delete(user)
            db.session.commit()

    room = Room.query.all()

    if room is not None:
        for user in room:
            db.session.delete(user)
            db.session.commit()

    device = Device.query.all()

    if device is not None:
        for user in device:
            db.session.delete(user)
            db.session.commit()

    shared_device = Shared_Device.query.all()

    if shared_device is not None:
        for user in shared_device:
            db.session.delete(user)
            db.session.commit()

    room_user = Room_User.query.all()

    if room_user is not None:
        for user in room_user:
            db.session.delete(user)
            db.session.commit()'''


    if g.user.is_authenticated():
        return render_template("my_house_shared_rooms.html", 
            title = g.user.nickname+"'s Smart House",
            rooms = g.user.rooms.order_by(Room.name).all(),
	        shared_devices = g.user.shared_devices.all(), 
            shared_rooms = g.user.shared_rooms.all())
    else:
        return redirect(url_for('home'))

@app.route('/myhouse_shared_devices/')
@login_required
def my_house_shared_devices():
    '''Shared devices'''
    if g.user.is_authenticated():
        return render_template("my_house_shared_devices.html", 
            title = g.user.nickname + "'s Smart House",
            rooms = g.user.rooms.order_by(Room.name).all(),
	        shared_devices = g.user.shared_devices.all(), 
            shared_rooms = g.user.shared_rooms.all())
    else:
        return redirect(url_for('home'))

####################################################################
############# User Log                                	############
####################################################################
def add_entry(user, message):
    '''Function used to create a message for each command used, i.e. logging'''
    u = User.query.get(user.id)
    if u:
        entry = Log_Entry(user=u, timestamp=datetime.now(), body = message)
        db.session.add(entry)
        db.session.commit()
        entries = user.log_entries.order_by(Log_Entry.timestamp).all()
        l = len(entries)
        if l > MAX_LOG_ENTRY: # max number of entries to keep
            for e in entries[0:l-MAX_LOG_ENTRY]:
                db.session.delete(e)
                db.session.commit()
        return True
    return False
	
@app.route('/log/<nickname>', methods =['GET', 'POST'])
@login_required
def log(nickname):
    '''Log page'''
    user = User.query.filter_by(nickname = nickname).first()
    if user == None or not user.is_authenticated:
        flash('User ' + nickname + ' not found.')
        return redirect(url_for('home'))
    return render_template("log.html", title="Log")

####################################################################
############# Devices                                   ############
####################################################################
@app.route('/devices/<nickname>', methods =['GET', 'POST'])
@login_required
def devices(nickname):
    '''List of devices in each room page'''
    user = User.query.filter_by(nickname = nickname).first()
    if user == None or not user.is_authenticated:
        flash('User ' + nickname + ' not found.')
        return redirect(url_for('home'))
    return render_template("devices.html", title="Devices", 
        rooms=user.rooms.order_by(Room.name).all(), 
        shared_devices=user.shared_devices.all(), 
        shared_rooms = user.shared_rooms.all()) 

@app.route('/devices/add/<nickname>', defaults={'room_name': 'house'}, 
    methods =['GET', 'POST'])
@app.route('/devices/add/<nickname>/<room_name>', methods =['GET', 'POST'])
@login_required
def device_add(nickname, room_name):
    '''Page to add a device'''
    user = User.query.filter_by(nickname = nickname).first()
    if user == None or not user.is_authenticated:
        flash('User ' + nickname + ' not found.')
        return redirect(url_for('home'))

    shared_room = Room_User.query.filter_by(room_name = room_name, 
        user_id = g.user.id, privilege = 1).first()
    owner_room = Room.query.filter_by(name = room_name, 
        user_id = g.user.id).first()

    if user.id != g.user.id:
        flash('You cannot add a device')
        return redirect(url_for('home'))
    allow_add_device = 0

    if shared_room is not None: 
        allow_add_device = allow_add_device + 1

    if owner_room is not None: 
        allow_add_device = allow_add_device + 1

    if(allow_add_device == 0):
        flash('You cannot add a device')
        return redirect(url_for('home'))

    form = DeviceForm('Device', room=room_name) #use default device name
    if form.validate_on_submit():
        msg = ""

        shared_room = (Room_User.query.filter_by(
            room_name = form.room.data.lower(), user_id = g.user.id).first())
        if shared_room is not None:
            room = Room.query.get(shared_room.room_id)

        elif form.room.data.lower() not in g.user.list_rooms(): 
        # room does not exist, create it
            room = Room(name = form.room.data.lower(), user = g.user,
                enable = True)
            msg = msg + 'Room ' + form.room.data.lower() + ' added. '
        else:
            room = (Room.query.filter_by(name=form.room.data.lower(), 
                user = g.user, user_id = g.user.id).first())
        db.session.add(room)
        db.session.commit()
        device = Device(name=form.name.data.lower(), 
            room = room, room_id = room.id, url = form.url.data, 
            request_type=form.request_type.data, 
            command = form.command.data,
            command_optional = form.command_optional.data,
            enable = form.enable.data,
            user = g.user,
            user_id = g.user.id,  
            message = "Created",
            last_used = datetime.now())
        db.session.add(device)
        db.session.commit()
        flash(msg+'Device '+device.name+' added.')
        add_entry(user, msg+'Device '+device.name+' added.')
        return redirect(url_for('devices', nickname = g.user.nickname))
    return render_template("device_form.html", 
        title = "New Device", form = form, 
        rooms = user.rooms.order_by(Room.name).all())
    
@app.route('/device/edit/<int:id>', methods =['GET', 'POST'])
@login_required
def device_edit(id):
    '''Page to edit devices'''
    device = Device.query.get(id)
    if device == None:
        flash('Device not found')
        return redirect(url_for('devices', nickname=g.user.nickname))

    shared_room = Room_User.query.filter_by(room_id = device.room_id, 
        user_id = g.user.id, privilege = 1).first()
    if device.user.id != g.user.id  and shared_room is None:
        flash('You cannot edit this device')
        return redirect(url_for('devices', nickname = g.user.nickname))
    form = DeviceForm(device.name, name = device.name, url = device.url, 
        request_type = device.request_type, command = device.command,
        command_optional = device.command_optional, enable = device.enable)
    if form.validate_on_submit():
        device.name = form.name.data.lower()
        device.url = form.url.data
        device.request_type = form.request_type.data
        device.command = form.command.data
        device.command_optional = form.command_optional.data
        device.enable = form.enable.data
        device.message = "Edited"
        device.last_used = datetime.now()
        db.session.add(device)
        db.session.commit()
        flash('Device '+device.name+' edited.')
        add_entry(g.user, 'Device '+device.name+' edited.')
        return redirect(url_for('devices', nickname = g.user.nickname))
    return render_template("device_form.html", title = "Edit Device", 
        form = form, rooms = g.user.rooms.order_by(Room.name).all())
	
@app.route('/device/delete/<int:id>')
@login_required
def device_delete(id):
    '''Page to delete devices'''
    device = Device.query.get(id)
    if device == None:
        flash('Device not found')
        return redirect(url_for('devices', nickname = g.user.nickname))

    shared_room = Room_User.query.filter_by(room_id = device.room_id, 
        user_id = g.user.id, privilege = 1).first()
    if device.user.id != g.user.id  and shared_room is None:
        flash('You cannot delete this device')
        return redirect(url_for('devices', nickname = g.user.nickname))
    name = device.name
    db.session.delete(device)
    db.session.commit()
    flash('Device '+name+' deleted.')
    add_entry(g.user, 'Device '+name+' deleted.')
    return redirect(url_for('devices', nickname = g.user.nickname))
	
@app.route('/device/enable/<int:id>', methods =['GET', 'POST'])
@login_required
def device_enable(id):
    '''Page to enable or disable devices'''
    device = Device.query.get(id)
    if device == None:
        flash('Device not found')
        return redirect(url_for('devices', nickname = g.user.nickname))

    shared_room = Room_User.query.filter_by(room_id = device.room_id, 
        user_id = g.user.id, privilege = 1).first()
    if device.user.id != g.user.id  and shared_room is None:
        flash('You cannot enable this device')
        return redirect(url_for('devices', nickname = g.user.nickname))
    device.enable = not device.enable
    if device.enable:
        flash('Device '+device.name+' enabled.')
        add_entry(g.user, 'Device '+device.name+' enabled.')
        device.message = "Enabled"
    else:
        flash('Device '+device.name+' disabled')
        add_entry(g.user, 'Device '+device.name+' disabled.')
        device.message = "Disabled"
    device.last_used = datetime.now()
    db.session.add(device)
    db.session.commit()
    return redirect(url_for('devices', nickname = g.user.nickname))

####################################################################
############# Rooms                                    	############
####################################################################	
@app.route('/room/add/<nickname>', methods = ['GET', 'POST'])
@login_required
def room_add(nickname):
    '''Page to add a room'''
    user = User.query.filter_by(nickname = nickname).first()
    if user == None or not user.is_authenticated:
        flash('User ' + nickname + ' not found.')
        return redirect(url_for('home'))
    if user.id != g.user.id:
        flash('You cannot add a room')
        return redirect(url_for('home'))
    form = RoomForm(original_name = 'House', user_id = user.id)#use default name
    if form.validate_on_submit():
        room = Room(name = form.name.data.lower(), 
            enable = form.enable.data, 
            user = user, 
            webcam_url = form.webcam_url.data)
        db.session.add(room)
        db.session.commit()
        flash('Room '+room.name+' added.')
        add_entry(g.user, 'Room '+room.name+' added.')
        return redirect(url_for('devices', nickname = g.user.nickname))
    return render_template("room_form.html", title = "New Room", 
        form = form, rooms = user.rooms.order_by(Room.name).all())

@app.route('/room/edit/<int:id>', methods =['GET', 'POST'])
@login_required
def room_edit(id):
    '''Page to edit a room'''
    room = Room.query.get(id)
    if room == None:
        flash('Room not found')
        return redirect(url_for('devices', nickname = g.user.nickname))

    shared_room = Room_User.query.filter_by(room_id = id, 
        user_id = g.user.id, privilege = 1).first()
    if room.user.id != g.user.id  and shared_room is None:
        flash('You cannot edit this room')
        return redirect(url_for('devices', nickname = g.user.nickname))
    form = RoomForm(room.name, 
        name = room.name, 
        enable = room.enable, 
        webcam_url = room.webcam_url)
    if form.validate_on_submit():
        room.name = form.name.data.lower()
        room.enable = form.enable.data
        room.webcam_url = form.webcam_url.data
        db.session.add(room)
        db.session.commit()
        flash('Room '+room.name+' edited.')
        add_entry(g.user, 'Room '+room.name+' edited.')
        return redirect(url_for('devices', nickname = g.user.nickname))
    return render_template("room_form.html", title="Edit Room", 
        form = form, rooms = g.user.rooms.order_by(Room.name).all())
	
@app.route('/room/delete/<int:id>')
@login_required
def room_delete(id):
    '''Function to delete a room and its devices'''
    room = Room.query.get(id)
    shared_room = Room_User.query.filter_by(room_id = id, privilege = 1).all()
    if room == None:
        flash('Room not found')
        return redirect(url_for('devices', 
            nickname = g.user.nickname))
    if room.user.id != g.user.id and shared_room is None:
        flash('You cannot delete this room')
        return redirect(url_for('devices', 
            nickname = g.user.nickname))
    for device in room.devices:
        add_entry(g.user, 'Device '+device.name+' deleted.')
        db.session.delete(device)
        db.session.commit()

    #delete users in shared room
    shared_room = Room_User.query.filter_by(room_id = id).all()
    if shared_room is not None:
        for user in shared_room:
            add_entry(user.room_user(), 'Room '+room.name+' deleted.')
            db.session.delete(user)
            db.session.commit()

    name = room.name
    db.session.delete(room)
    db.session.commit()

    flash('Room '+name+' and contained devices deleted.')
    add_entry(g.user, 'Room '+name+' and contained devices deleted.')
    return redirect(url_for('devices', 
        nickname = g.user.nickname))

@app.route('/room/enable/<int:id>', methods =['GET', 'POST'])
@login_required
def room_enable(id):
    '''Page to enable or disable a room and its devices'''
    room = Room.query.get(id)
    if room == None:
        flash('Room not found')
        return redirect(url_for('devices', 
            nickname = g.user.nickname))
    shared_room = Room_User.query.filter_by(room_id = id, 
        user_id = g.user.id, privilege = 1).first()
    if room.user.id != g.user.id and shared_room is None:
        flash('You cannot enable this room')
        return redirect(url_for('devices', 
            nickname = g.user.nickname))
    room.enable = not room.enable
    db.session.add(room)
    for device in room.devices:
        device.enable = room.enable
        db.session.add(device)
    db.session.commit()
    if room.enable:
        flash('Room '+room.name+' and its contained devices enabled.')
        add_entry(g.user, 'Room '+room.name+' and its contained '+
            'devices enabled.')
        return redirect(url_for('devices', nickname = g.user.nickname))
    else:
        flash('Room '+room.name+' and its contained devices disabled')
        add_entry(g.user, 'Room '+room.name+' and its contained '+
            'devices disabled.')
    return redirect(url_for('devices', nickname = g.user.nickname))

####################################################################
############# User Profile                              ############
####################################################################
@app.route('/user/<nickname>', methods = ['GET','POST'])
@login_required
def user(nickname):
    '''Profile page'''
    user = User.query.filter_by(nickname = nickname).first()
    if user == None:
        flash('User ' + nickname + ' not found.')
        return redirect(url_for('home'))
    elif user.status == 'Delete1' or user.status == 'Delete2':
        flash(nickname + '\'s account has been deactivated!.')
        return redirect(url_for('home'))
    form = PostForm()
    if form.validate_on_submit():
        post = Post(body = form.post.data, timestamp = datetime.now(), 
            author = user)
        db.session.add(post)
        db.session.commit()
        flash('Post posted.')
        return redirect(url_for('user', nickname = user.nickname))
    if g.user.is_authenticated():
        posts = user.get_posts()
    else:
        posts = []
    return render_template('user.html', user = user, 
        posts = posts, form = form)	

@app.route('/edit', methods = ['GET', 'POST'])
@login_required
def edit():
    '''Page to edit User's profile'''
    form = EditForm(g.user.nickname)
    if form.validate_on_submit():
        g.user.nickname = form.nickname.data
        g.user.about_me = form.about_me.data
        g.user.first_name = form.first_name.data
        g.user.last_name = form.last_name.data
        g.user.email = form.email.data
        db.session.add(g.user)
        db.session.commit()
        flash('Your changes have been saved.')
        return redirect(url_for('user', nickname = g.user.nickname))
    else:
        form.nickname.data = g.user.nickname
        form.about_me.data = g.user.about_me
    form.first_name.data = g.user.first_name
    form.last_name.data = g.user.last_name
    form.email.data = g.user.email
    return render_template('edit.html', form = form)
    
####################################################################
############# Login/Logout & Registration               ############
####################################################################
@app.route('/login', methods =['GET', 'POST'])
@oid.loginhandler
def login():
    '''Login page'''
    if g.user is not None and g.user.is_authenticated():
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        session['remember_me'] = form.remember_me.data
        return oid.try_login(form.openid.data, ask_for = ['nickname', 'email'])
    return render_template("login.html", title="Sign In", 
        form = form, providers = app.config['OPENID_PROVIDERS']) 

@app.route('/logout')
def logout():
    '''Log out function'''
    logout_user()
    flash("You have been successfully logged out!")
    return redirect(url_for('home'))

@app.before_request
def before_request():
    '''Function before request'''
    g.user = current_user
    if g.user.is_authenticated():
        g.user.last_seen = datetime.now()
        db.session.add(g.user)
        db.session.commit()

@lm.user_loader
def load_user(id):
    '''Function to load user'''
    return User.query.get(int(id))

@oid.after_login
def after_login(resp):
    '''Function after login'''
    if resp.email is None or resp.email == "":
        flash('Invalid login. Please try again.')
        redirect(url_for('login'))
    user = User.query.filter_by(email = resp.email).first()
    if user is None:
        nickname = resp.nickname
        if nickname is None or nickname == "":
            nickname = resp.email.split('@')[0]
        nickname = User.make_unique_nickname(nickname)
        user = User(nickname = nickname, email = resp.email, role = ROLE_USER)
        db.session.add(user)
        db.session.commit()
    remember_me = False
    if 'remember_me' in session:
        remember_me = session['remember_me']
        session.pop('remember_me', None)
    login_user(user, remember = remember_me)

    #don't login in user if he/she was kicked out by admin
    if(g.user.status == 'Disabled'):
        return redirect(url_for('disabled'))
    elif(g.user.status == 'Delete1'):
        g.user.status = 'Enabled'
        db.session.commit()
        flash('Welcome Back %s' % user.nickname)
        return redirect(url_for('home'))
    elif(g.user.status == 'Delete2'):
        g.user.status = 'Disabled'
        db.session.commit()
        return redirect(url_for('disabled'))
    else:
        flash('You have been successfully logged in as %s' % user.nickname)
        return redirect(url_for('home'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    '''Registration page'''
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(nickname=form.nickname.data, 
            email = form.email.data, role = ROLE_USER, 
            first_name = form.first_name.data, 
            last_name = form.last_name.data, 
            password = form.password.data, 
            date_registered = datetime.now())
        db.session.add(user)
        db.session.commit()
        flash('Congratulations!  Your registration was successful!')
        return redirect(url_for('signin'))
    return render_template('register.html', form=form)

@app.route('/signin', methods=['GET', 'POST'])
def signin():
    '''Sign in page'''
    if g.user is not None and g.user.is_authenticated():
        return redirect(url_for('home'))
    form = LoginForm2()
    if form.validate_on_submit():
        remember_me = False
        if 'remember_me' in session:
            remember_me = session['remember_me']
            session.pop('remember_me', None)
        login_user(form.user, remember = remember_me)
        
        #don't login in user if he/she was kicked out by admin
        if(g.user.status == 'Disabled'):
            return redirect(url_for('disabled'))

        elif(g.user.status == 'Delete1'):
            g.user.status = 'Enabled'
            db.session.commit()
            fullname = form.user.first_name + ' ' + form.user.last_name
            flash('Welcome Back %s' % fullname)
            return redirect(url_for('home'))
            
        elif(g.user.status=='Delete2'):
            g.user.status = 'Disabled'
            db.session.commit()
            return redirect(url_for('disabled'))

        else:
            fullname = form.user.first_name + ' ' + form.user.last_name
            flash('You have been successfully logged in as %s' % fullname)
            session['user_id'] = form.user.id
            return redirect(url_for('home'))
    return render_template('signin.html', form = form)

####################################################################
############# In-Out Messages/commands                  ############
####################################################################
@app.route('/outmsg/<room>/<device>/<command>/<request_type>')
@login_required
def outmsg(room, device, command, request_type):
    '''Function to log each command and send a request'''
    room = str(room)
    device = str(device)
    command = str(command)
    request_type = str(request_type).upper()
    if g.user.is_authenticated():
        r = g.user.rooms.filter_by(name=room).first()
        if r:
            if r.user.id != g.user.id:
                flash('You cannot control this room.')
            else:
                d = r.devices.filter_by(name=device).first()
            if d:
                if (d.command.lower() == command.lower() or 
                    d.command_optional.lower() == command.lower()):
                    add_entry(g.user, room+' '+device +' '+command)
                    d.message = room+' '+device +' '+command
                    d.last_used = datetime.now()
                    db.session.add(d)
                    db.session.commit()
                    r = make_request(d.url+'/'+command, d.request_type)
                    if (r is None):
                        flash('Improper request type.')
                    else:
                        flash('Command response: #'+str(r.status_code)+
                            ' | Text: '+str(r.text))
                else:
                    flash('Unknown device command '+command)
            else:
                flash('Unknown device '+device)
        else:
            flash('Unknown room '+room)
        return redirect(url_for('my_house'))
    return render_template(url_for('home'))

def make_request(url, request_type):
    '''Function to create a request'''
    if request_type == 'GET':
        try:
            r = requests.get(url)
        except:
            return None
    elif request_type == 'POST':
        try:
            r = requests.get(url)
        except:
            return None
    else:
        return None
    return r
	
@app.route('/inmsg/<username>/<room>/<device>/<command>',
    defaults={'msg': ''}, methods=['POST','GET'])
@app.route('/inmsg/<username>/<room>/<device>/<command>/<msg>', 
    methods=['POST', 'GET'])
def inmsg(username, room, device, command, msg):
    '''Function to get a message and log it for a 
    specific room-device-command set.'''
    username = str(username)
    room = str(room)
    device = str(device)
    command = str(command)
    msg = str(msg)
    u = User.query.filter_by(nickname = username).first()
    if u:
        r = u.rooms.filter_by(name = room.lower()).first()
        if r:
            d = r.devices.filter_by(name = device.lower()).first()
            if d:
                if (d.command == command.lower() or 
                    d.command_optional == command.lower()):
                    d.last_used = datetime.now()
                    d.message = device +' '+command+' '+msg
                    add_entry(u, '[external]: '+device +' '+command+' '+msg)
                    db.session.add(d)
                    db.session.commit()
                    return "Request accepted."
    return "Incorrect input request."

####################################################################
############# Error Handling                            ############
####################################################################
@app.errorhandler(404)
def internal_error(error):
    '''404 error page'''
    return render_template('404.html'), 404

@app.errorhandler(500)
def internal_error(error):
    '''500 error page'''
    db.session.rollback() # roll back to previous database state
    return render_template('500.html'), 500

####################################################################
############# Admin Account                           	############
####################################################################
@app.route('/admin/enable_account/<int:id>')
def enable_account(id):
    '''Page to enable a user account'''
    user = User.query.get(id)
    user.status = 'Enabled'
    db.session.commit()
    message = user.nickname + "\'s account has been enabled!"
    flash(message)
    return redirect(url_for('manage_users'))

@app.route('/admin/disable_account/<int:id>')
def disable_account(id):
    '''Page to disable a user account'''
    user = User.query.get(id)
    user.status = 'Disabled'
    db.session.commit()
    message = user.nickname + "\'s account has been disabled!"
    flash(message)
    return redirect(url_for('manage_users'))

def check_admin(user):
    '''Function to check if user is admin'''
    if user is not None and user.is_authenticated():
        if user.role == ROLE_ADMIN:
            return 0
    return 1

@app.route('/manage_users')
def manage_users():
    '''Page to manage user accounts'''
    flag = check_admin(g.user)
    if flag == 1:
        return redirect(url_for('access_denied'))
    else:
        users = User.query.filter_by(role = ROLE_USER).all()
    return render_template('manage_users.html', users = users)
 
@app.route('/access_denied')
def access_denied():
    '''Forbidden access page'''
    return render_template('access_denied.html')

@app.route('/add_admin', methods = ['GET', 'POST'])
def add_admin():
    '''Page to add admin account'''
    flag = check_admin(g.user)
    if flag == 1:
        return redirect(url_for('access_denied'))
    else:
        form = AddAdminForm()
        if form.validate_on_submit():
            user = User.query.filter_by(email = form.email.data).first()
            user.role = ROLE_ADMIN
            db.session.commit()
            message = form.email.data + " has been appointed as an admin!"
            flash(message)
            return redirect(url_for('admins'))
        return render_template('add_admin.html', form = form)

@app.route('/admins')
def admins():
    '''Page to view admin accounts'''
    flag = check_admin(g.user)
    if flag == 1:
        return redirect(url_for('access_denied'))
    else:
        users = User.query.filter_by(role=ROLE_ADMIN).all()
        return render_template('admins.html', users = users)

@app.route('/disabled')
def disabled():
    '''Page for disabled users'''
    logout_user()         
    return render_template('disabled.html')

####################################################################
############# Account Settings                          ############
####################################################################

@app.route('/password', methods=['GET', 'POST'])     
def password():
    '''Pasword reset page'''
    form = EditPasswordForm()
    if form.validate_on_submit():
        g.user.password = form.new_password.data
        db.session.commit()
        flash("Your password has been successfully changed!")
        return redirect(url_for('user', nickname = g.user.nickname))

    return render_template('password.html', form = form)

@app.route('/user/deactivate-account/')
def deactivate_account():
    '''Page to delete or deactivate account'''
    if g.user.status == 'Enabled':
        g.user.status = 'Delete1'
        db.session.commit()
    elif g.user.status == 'Disabled':
        g.user.status = 'Delete2'
        db.session.commit()
    flash("Your account has been deactivated")
    logout_user()  
    return redirect(url_for('home'))


####################################################################
############# Sharing Devices                           ############
####################################################################

@app.route('/user/share-device/<int:id>', methods=['GET', 'POST'])
def share_device(id):
    '''Page to share a device'''
    g.device_id = id
    form = SharedDeviceForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        device = Device.query.get(id)
        room = Room.query.get(1)
        shared_device = Shared_Device(user=user, 
	    device=device, owner_id=g.user.id) 
        db.session.add(shared_device)
        db.session.commit()
        msg = (device.name + ' has been successfully shared with ' + 
            form.email.data)
        device_owner = ('You shared ' + device.name  + ' with ' + 
            form.email.data)
        receiver = (g.user.email + ' shared ' + device.name + 
            ' device with you.') 
        flash(msg)
        add_entry(g.user, device_owner)
        add_entry(user, receiver)
        return redirect(url_for('devices', nickname = g.user.nickname))
    return render_template('share_device.html', form = form)
        

@app.route('/delete-shared-device/<int:id>')
def remove_shared_device(id):
    '''Page to remove a shared device'''
    shared_device = Shared_Device.query.get(id)
    device_owner = User.query.get(shared_device.owner_id)
    db.session.delete(shared_device)
    db.session.commit()
    msg = shared_device.device.name  + ' has been successfully removed!'
    device_owner = (g.user.nickname + ' removed ' + 
        shared_device.device.name + ' from her list.') 
    receiver = (shared_device.device.name  + ' was removed '+
        'from your device list.')
    #add_entry(device_owner,device_owner)
    add_entry(g.user, receiver)
    flash(msg)
    return redirect(url_for('home'))


@app.route('/shared_devices/<nickname>', methods =['GET', 'POST'])
@login_required
def shared_devices(nickname):
    '''Page to remove a shared device'''
    user = User.query.filter_by(nickname = nickname).first()
    if user == None or not user.is_authenticated:
        flash('User ' + nickname + ' not found.')
        return redirect(url_for('home'))
    return render_template("shared_devices_page.html", 
        shared_devices=user.shared_devices.all()) 

####################################################################
############# Sharing Rooms                             ############
####################################################################

@app.route('/rooms/add-users/<int:room_id>', methods = ['GET', 'POST'])
def add_room_users(room_id):
    '''Page to add a room's users'''
    g.room_id = room_id
    form = AddRoomUsersForm()
    if form.add_another_user.data:
        form.users.append_entry()

    if form.validate_on_submit():
        for user in form.users:
            new_user = User.query.filter_by(email = user.email.data).first()
            room = Room.query.get(room_id)
            room_user = Room_User(user_id = new_user.id, room_id = room_id, 
                room_name = room.name, privilege = int(user.privilege.data))
            db.session.add(room_user)
            db.session.commit()
        flash("Users successfully added to room")
        return redirect(url_for('devices', nickname = g.user.nickname))
    return render_template('add_room_users.html', form = form)


@app.route('/rooms/manage-users/<int:room_id>', methods = ['GET', 'POST'])
def manage_room_users(room_id):
    '''Page to manage room users'''
    room = Room.query.get(room_id)
    users = Room_User.query.filter_by(room_id = room_id).all()
    for u in users:
        print u.user_id, u.room_user().nickname
    return render_template('manage_room_users.html', room = room, users = users)

@app.route('/rooms/remove-user/<int:RoomUser_id>')
def remove_room_user(RoomUser_id):
    '''Page to remove room users'''
    #RoomUser_id is Room_User.id, not Room_User.user_id
    shared_room = Room_User.query.get(RoomUser_id)
    user = User.query.get(shared_room.user_id)
    msg = user.email + " was successfully deleted!"
    db.session.delete(shared_room)
    db.session.commit()
    flash(msg)
    return redirect(url_for('devices', nickname = g.user.nickname))

@app.route('/rooms/update-user/', methods = ['GET', 'POST'])
def update_room_user():
    '''Page to edit the room users'''
    if request.method == 'POST':
        role = int(request.form['privilege'])
        RoomUser_id = int(request.form['RoomUser_id'])
        room_id = int(request.form['room_id'])
        shared_room = Room_User.query.get(RoomUser_id)
        user = User.query.get(shared_room.user_id)
        msg = user.email + "'s role was successfully updated!"
        shared_room.privilege = role
        db.session.commit()
        flash(msg)
        return redirect(url_for('manage_room_users', room_id = room_id))

@app.route('/shared_rooms/<nickname>', methods =['GET', 'POST'])
@login_required
def shared_rooms(nickname):
    '''Page to see shared rooms'''
    user = User.query.filter_by(nickname = nickname).first()
    if user == None or not user.is_authenticated:
        flash('User ' + nickname + ' not found.')
        return redirect(url_for('home'))
    return render_template("shared_rooms_page.html", 
         shared_rooms = user.shared_rooms.all()) 

@app.route('/rooms/remove-shared-room/<int:user_id>')
def remove_shared_room(user_id):
    '''Page to delete a shared room'''
    shared_room = Room_User.query.filter_by(user_id = user_id).first()
    user = User.query.get(user_id)
    msg = shared_room.room_name + " was successfully removed!"
    db.session.delete(shared_room)
    db.session.commit()
    flash(msg)
    return redirect(url_for('shared_rooms', nickname = g.user.nickname))

####################################################################
############# Forum                                     ############
####################################################################

@app.route('/create_forum', methods = ['GET', 'POST'])
def create_forum():
    '''Page to create a new forum topic'''
    form = CreateForumForm()
    if form.validate_on_submit():
        current_time = datetime.now()
        forum = (Forum(creator_id = g.user.id, title = form.title.data, 
            date_created = current_time))
        db.session.add(forum)
        db.session.commit()
        forum_post = Forum_Post(forum_id = forum.id, user_id = g.user.id,
            body = form.body.data, date_added = current_time)
        db.session.add(forum_post)
        db.session.commit()
        flash("Post successfully created!")
        return redirect(url_for('forum'))
    return render_template('create_forum.html', form = form)

@app.route('/forum')
def forum():
    '''Forum page'''
    forums = Forum.query.all()
    return render_template('forum.html', forums = forums)

@app.route('/forum-posts/<int:forum_id>')
def forum_posts(forum_id):
    '''Page to view a forum's posts'''
    forum_posts = Forum_Post.query.filter_by(forum_id = forum_id).all()
    forum = Forum.query.get(forum_id)
    return render_template('forum_posts.html', forum_posts = forum_posts,
        title = forum.title, forum_id = forum.id)

@app.route('/reply-post/<int:forum_id>', methods = ['GET', 'POST'])
def reply_post(forum_id):
    '''Page to reply to forum posts'''
    form = ForumForm()
    if form.validate_on_submit():
        forum_post = Forum_Post(forum_id = forum_id, user_id = g.user.id,
            body = form.body.data, date_added = datetime.now())
        db.session.add(forum_post)
        db.session.commit()
        flash("Post successfully added!")
        return redirect(url_for('forum_posts', forum_id = forum_id))
    return render_template('reply_post.html', form = form)

@app.route('/delete-post/<int:forum_id>')
def delete_post(forum_id):
    '''function to delete forum posts'''
    forum = Forum.query.get(forum_id)
    forum_posts = Forum_Post.query.filter_by(forum_id = forum_id)
    for comment in forum_posts:
        db.session.delete(comment)
        db.session.commit()

    db.session.delete(forum)
    db.session.commit()
    flash("Post successfully deleted!")
    return redirect(url_for('forum'))

@app.route('/delete-comment/<int:forum_id>, <int:comment_id>')
def delete_comment(forum_id, comment_id):
    '''function to delete forum post comment'''
    forum_post = (Forum_Post.query.filter_by(id = comment_id, 
        forum_id = forum_id).first())
    db.session.delete(forum_post)
    db.session.commit()
    flash("Comment successfully deleted!")
    return redirect(url_for('forum_posts', forum_id = forum_id))

@app.route('/my_forum_posts')
def my_forum_posts():
    '''Page to view user's forum posts'''
    forums = Forum.query.filter_by(creator_id = g.user.id).all()
    return render_template('my_forum_posts.html', forums = forums)


####################################################################
############# User to User Direct Messaging             ############
####################################################################


@app.route('/create-message/<nickname>', methods = ['GET', 'POST'])
def create_message(nickname):
    '''Page to create a message'''
    form = CreateMessageForm()
    if form.validate_on_submit():
        user = User.query.filter_by(nickname = nickname).first()
        current_time = datetime.now()

        message_sender = User_Messages(user_id = g.user.id, 
            title = form.title.data, date = current_time)
        db.session.add(message_sender)
        db.session.commit()

        message_recipient = User_Messages(linked_message_id =message_sender.id,
            user_id = user.id, title = form.title.data, date = current_time)
        db.session.add(message_recipient)
        db.session.commit()

        message_sender.linked_message_id = message_recipient.id
        db.session.commit()

        message_thread_sender = Message_Thread(message_id = message_sender.id, 
            user_id = g.user.id, other_user_id = user.id , role = ROLE_SENDER, 
            body = form.body.data, date = current_time)
        db.session.add(message_thread_sender)
        db.session.commit()

        message_thread_recipient = Message_Thread(
            message_id = message_recipient.id,
            user_id = user.id, other_user_id = g.user.id, 
            role = ROLE_RECIPIENT, body = form.body.data, date = current_time)
        db.session.add(message_thread_recipient)
        db.session.commit()

        flash("Message successfully sent!")
        return redirect(url_for('user', nickname = nickname))
    return render_template('create_message.html', 
        form = form, nickname = nickname)

@app.route('/messages')
def messages():
    '''Page to view messages'''

    messages = User_Messages.query.filter_by(user_id = g.user.id).all()
    return render_template('messages.html', messages = messages)


@app.route('/message/<int:message_id>')
def message(message_id):
    '''Page to view specific message'''
    message_threads = Message_Thread.query.filter_by(message_id = message_id,
        user_id = g.user.id).all()
    message = User_Messages.query.get(message_id)
    return render_template('message.html', message_threads = message_threads,
        title = message.title, message_id = message_id)


@app.route('/reply-message/<int:message_id>,<int:user_id>', 
    methods = ['GET', 'POST'])
def reply_message(message_id, user_id):
    '''Page to reply to a message'''
    form = ReplyMessageForm()
    if form.validate_on_submit():
        message = User_Messages.query.get(message_id)
        linked_message_id = message.linked_message_id
        linked_message = User_Messages.query.get(linked_message_id)
    
        if (linked_message is None):
            message_recipient = User_Messages(linked_message_id = message.id,
            user_id = user_id, title = message.title, date = message.date)
            db.session.add(message_recipient)
            db.session.commit()
            message.linked_message_id = message_recipient.id
            db.session.commit()
        else:
            message_recipient = linked_message
    
        current_time = datetime.now()

        message_thread_sender = Message_Thread(message_id = message_id, 
            user_id = g.user.id, other_user_id= user_id, role = ROLE_SENDER,
            body = form.body.data, date = current_time)
        db.session.add(message_thread_sender)
        db.session.commit()

        message_thread_recipient = Message_Thread(
            message_id = message_recipient.id,
            user_id = user_id, 
            other_user_id = g.user.id, 
            role = ROLE_RECIPIENT,
            body = form.body.data, date = current_time)
        db.session.add(message_thread_recipient)
        db.session.commit()

        flash("Message successfully sent!")
        return redirect(url_for('message', message_id = message_id))
    return render_template('reply_message.html', form = form)

@app.route('/delete-message/<int:message_id>')
def delete_message(message_id):
    '''Page to delete a message'''
    message = User_Messages.query.filter_by(id = message_id).first()
    message_threads = (Message_Thread.query.filter_by(message_id = 
        message.id).all())
    db.session.delete(message)
    db.session.commit()
    for msg in message_threads:
        db.session.delete(msg)
        db.session.commit()
    flash("Message successfully deleted!")
    return redirect(url_for('messages'))


####################################################################
############# Contact Page                              ############
####################################################################

@app.route('/contact-us', methods = ['GET', 'POST'])
def contact():
    '''Page to contact webmasters'''
    form = ContactForm()
    if form.validate_on_submit():
        content = ''
        if g.user is not None and g.user.is_authenticated():
            content = '<p> <b>User ID: </b?' + str(g.user.id) + '</p>'
            link = url_for('user', nickname = g.user.nickname,  _external=True)
            content = content + '<p><a href = "' + link + '"> Profile: </a></p>'

        content = (content + '<p> <b>Email Address: </b>' + 
            str(form.email.data) + '</p>')
        content = content + '<b> Message <b> <br>'
        content = content + '<p>' + str(form.body.data) + '</p>'
        msg = Message('Contact Us', sender = "smarthouseapp@gmail.com", 
            recipients = ["smarthouseapp@gmail.com"])
        msg.html = content
        mail.send(msg)
        flash("Message successfully sent!")
        return redirect(url_for('home'))
    return render_template('contact.html', form = form)
    

####################################################################
############# Privacy Page                              ############
####################################################################

@app.route('/privacy')
def privacy():
    '''Privacy Page'''
    return render_template('privacy.html')
    

####################################################################
############# Miscellaneous                             ############
####################################################################
@app.route('/register_mike')
def register_mike():
    '''Run this to register Mike's details (needed for acceptance tests)'''
    user = User.query.filter_by(email = "m@gmail.com").first()
    if user is None:
        user = User(nickname = "ms", email = "m@gmail.com", role = 1,
 	    first_name = "Mike", last_name = "Simic", password = "tes", 
	    date_registered=datetime.utcnow())
        db.session.add(user)
        db.session.commit()
        msg = "Mike successfully registered!"
    else:
        msg = "Account already exists!"

    flash(msg)
    return redirect(url_for('home'))


@app.route('/delete_mike/<email_address>', methods = ['GET', 'POST'])
def delete_mike():
    '''Run this to delete Mike's details (needed for acceptance tests)'''
    user = User.query.filter_by(email = address).first()
    if user is not None:
        db.session.delete(user)
        db.session.commit()

    msg = "Account successfully deleted!"

    flash(msg)
    return redirect(url_for('home'))


