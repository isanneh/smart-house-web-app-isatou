import requests



def log_IO_correct_UP(url, user, password):  #function name means log in/out with correct Username and Password
	data={'email': user, 'password': password}
	request_data = requests.post(url, data, allow_redirects=True)
	assert "You have been successfully logged in as" in request_data.content
	r= requests.post(request_data.url, allow_redirects=True)
	assert "Welcome" in r.content



def register_test(url, fname, lname, email, username, password, login_url):

	data={'first_name': fname, 'last_name': lname, 'email': email, 'nickname': username, 'password': password, 'confirm_password': password}
	r = requests.post(url, data, allow_redirects=True)
	assert "Congratulations! Your registration was successful!" in r.content
	log_in_out_test(login_url, email, password)