#!shwa_env/bin/python
import os
import unittest
from config import basedir
from app import app, db, models
from app.models import User, Device, Room, Shared_Device
from datetime import datetime

class TestCaseDatabase(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'test.db')
        self.app = app.test_client()
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()


    def test_avatar(self):
        u = User(nickname = 'john', email = 'john@example.com')
        avatar = str(u.avatar(128))
        expected = 'http://gravatar.com/avatar/d4c74594d841139328695756648b6bd6'
        self.assertEqual(avatar[0:len(expected)],expected)

    def test_make_unique_nickname(self):
        u = User(nickname = 'john', email = 'john@example.com')
        db.session.add(u)
        db.session.commit()
        nickname = User.make_unique_nickname('john')
        self.assertNotEqual( nickname, 'john')
        u = User(nickname = nickname, email = 'susan@example.com')
        db.session.add(u)
        db.session.commit()
        nickname2 = User.make_unique_nickname('john')
        self.assertNotEqual( nickname2, 'john')
        self.assertNotEqual( nickname2, nickname)

    def test_device_add(self):
        u = User(nickname = 'David', email = 'david@example.com')
        db.session.add(u)
        db.session.commit()
        #device has no room parameter
        d = Device(name="lights",command="on",url="http://www.google.com",request_type="GET", enable=True, user=u)
        db.session.add(d)
        db.session.commit()
        device = models.Device.query.filter_by(name="lights").first()
        self.assertTrue(device)
        self.assertEqual("on",device.command)
        self.assertEqual("http://www.google.com", device.url)
        self.assertEqual("GET", device.request_type)
        self.assertTrue(device.enable)
        self.assertEqual(device.user.id,u.id)
        db.session.delete(d)
        db.session.delete(u)
        db.session.commit()

    def test_device_edit(self):
        u = User(nickname = 'Michael', email = 'michael@example.com')
        db.session.add(u)
        db.session.commit()
        #device has no room parameter
        d = Device(name="AC",command="low",url="http://www.google.com",request_type="GET", enable=True, user=u)
        db.session.add(d)
        db.session.commit()
        device = models.Device.query.filter_by(name="AC").first()
        device.name = "changed"
        device.command = "changed"
        device.url = "http://changed"
        device.enable = False
        db.session.add(device)
        db.session.commit()
        device = models.Device.query.filter_by(name="changed").first()
        self.assertTrue(device)
        self.assertEqual("changed", device.command)
        self.assertEqual("http://changed", device.url)
        self.assertEqual( "GET", device.request_type)
        self.assertFalse(device.enable)
        self.assertEqual(device.user.id,u.id)
        db.session.delete(device)
        db.session.delete(u)
        db.session.commit()

    def test_devie_delete(self):
        u = User(nickname = 'karim', email = 'karim@example.com')
        db.session.add(u)
        db.session.commit()
        #device has no room parameter
        d = Device(name="charger",command="off",url="http://www.google.com",request_type="GET", enable=True, user=u)
        db.session.add(d)
        db.session.commit()
        device = models.Device.query.filter_by(name="charger").first()
        self.assertTrue(device)
        assert len(u.devices.all()) == 1
        db.session.delete(device)
        db.session.commit()
        self.assertEqual(len(u.devices.all()),0)
        db.session.delete(u)
        db.session.commit()

    def test_device_enable(self):
        u = User(nickname = 'Fernando', email = 'fernando@example.com')
        db.session.add(u)
        db.session.commit()
        #device has no room parameter
        d = Device(name="door",command="lock",url="http://www.google.com",request_type="GET", enable=True, user=u)
        db.session.add(d)
        db.session.commit()
        device = models.Device.query.filter_by(name="door").first()
        self.assertTrue(device)
        self.assertTrue(device.enable)
        device.enable = False
        db.session.add(device)
        db.session.commit()
        device = models.Device.query.filter_by(name="door").first()
        self.assertFalse(device.enable)
        device.enable = True
        db.session.add(device)
        db.session.commit()
        device = models.Device.query.filter_by(name="door").first()
        self.assertTrue(device.enable)
        db.session.delete(device)
        db.session.delete(u)
        db.session.commit()

    def test_room_add(self):
        u = User(nickname = 'Volta', email = 'volta@example.com')
        db.session.add(u)
        db.session.commit()
        self.assertEqual(len(u.rooms.all()),0)
        r = Room(enable=True,name='cage',user=u)
        db.session.add(r)
        db.session.commit()
        self.assertEqual(len(u.rooms.all()),1)
        room = models.Room.query.filter_by(name='cage').first()
        self.assertTrue(room)
        self.assertEqual('Volta',room.user.nickname)
        self.assertTrue(room.enable)
        db.session.delete(room)
        db.session.delete(u)
        db.session.commit()

    def test_room_delete(self):
        u = User(nickname = 'SuperMario', email = 'supermario@example.com')
        db.session.add(u)
        r = Room(enable=True,name='castle',user=u)
        db.session.add(r)
        db.session.commit()
        self.assertEqual(len(u.rooms.all()),1)
        room = models.Room.query.filter_by(name='castle').first()
        assert room
        db.session.delete(room)
        db.session.commit()
        self.assertEqual(len(u.rooms.all()),0)
        db.session.delete(u)
        db.session.commit()

    def test_room_enable(self):
        u = User(nickname = 'Yoshi', email = 'yoshi@example.com')
        db.session.add(u)
        r = Room(enable=True,name='fruit',user=u)
        db.session.add(r)
        db.session.commit()
        self.assertTrue(r.enable)
        room = models.Room.query.filter_by(name='fruit').first()
        self.assertTrue(room)
        room.enable = False
        db.session.add(room)
        db.session.commit()
        self.assertFalse(room.enable)
        db.session.delete(room)
        db.session.delete(u)
        db.session.commit()

    def test_unique_room_name(self):
        u = User(nickname = 'Maggie', email = 'maggie@example.com')
        db.session.add(u)
        r = Room(enable=True,name='chicuti',user=u)
        db.session.add(r)
        db.session.commit()
        rname = Room.make_unique_name('chicuti')
        self.assertNotEqual(rname,'chicuti')
        r2 = Room(name = rname, enable=True,user=u)
        db.session.add(r2)
        db.session.commit()
        room2 = Room.query.filter_by(name=rname).first()
        self.assertNotEqual(room2.name,'chicuti')
        db.session.delete(r)
        db.session.delete(r2)
        db.session.delete(u)
        db.session.commit()

    def test_unique_device_name_in_room(self):
        u = User(nickname = 'Paco', email = 'paco@example.com')
        db.session.add(u)
        r = Room(enable=True,name='jaula',user=u)
        db.session.add(r)
        d = Device(name='food',user=u,room=r, enable=True,url='http://rara',command='give',request_type='GET')
        db.session.add(d)
        db.session.commit()
        dname = Device.make_unique_name('food')
        self.assertNotEqual(dname,'food')
        d2 = Device(name=dname,user=u,room=r, enable=True,url='http://rara',command='give',request_type='GET')
        db.session.add(d2)
        db.session.commit()
        device2 = Device.query.filter_by(name=dname).first()
        self.assertNotEqual(device2.name, 'food')
        db.session.delete(d)
        db.session.delete(d2)
        db.session.delete(r)
        db.session.delete(u)
        db.session.commit()

    def test_log_entry(self):
        pass

    def test_account_settings(self):
        user=User(first_name='Vicky', last_name='Doe', email='testuser4@example.com', nickname='testuser4', password='test')
        db.session.add(user)
        db.session.commit()
        user.first_name = "Angie"
        user.last_name = "Wong"
        user.email = "angie@example.com"
        user.nickname = "angie"
        user.about_me = "Love electronics"
        db.session.commit()
        edited=User.query.filter_by(id=user.id).first()
        self.assertTrue( edited.first_name ==  "Angie")
        self.assertTrue( edited.last_name ==  "Wong")
        self.assertTrue( edited.email ==  "angie@example.com")
        self.assertTrue( edited.nickname ==  "angie")
        self.assertTrue( edited.about_me ==  "Love electronics")
        db.session.delete(edited)
        db.session.commit()

    def test_account_delete(self):
        user = User(first_name='Vicky', last_name='Doe', email='testuser4@example.com', nickname='testuser4', password='test')
        db.session.add(user)
        db.session.commit()
        user_id = user.id
        db.session.delete(user)
        db.session.commit()
        deleted=User.query.filter_by(id=user_id).first()
        self.assertTrue( deleted is None)

    def test_share_device(self):
        user = User(first_name='Vicky', last_name='Doe', email='testuser4@example.com', nickname='testuser4', password='test')
        db.session.add(user)
        db.session.commit()
        owner=User(first_name='Eric', last_name='Doe', email='testuser5@example.com', nickname='testuser5', password='test')
        db.session.add(owner)
        db.session.commit()

        device = Device(name="lights",command="on",url="http://www.google.com",request_type="GET", enable=True, user=owner)
        db.session.add(device)
        db.session.commit()

        #adding shared device
        shared_device = Shared_Device(user=user, device=device, owner_id=owner.id)
        db.session.add(shared_device)
        db.session.commit()

        shared_device = Shared_Device.query.filter_by(user_id = user.id).first()
        self.assertTrue( shared_device.device == device)

        shared_device_id = shared_device.id

        #deleting shared device
        db.session.delete(shared_device)
        db.session.commit()
        deleted = Shared_Device.query.filter_by(id=shared_device_id).first()
        self.assertTrue( deleted is None)

    def test_delete_shared_device(self):
        user = User(first_name='Vicky', last_name='Doe', email='testuser4@example.com', nickname='testuser4', password='test')
        db.session.add(user)
        db.session.commit()
        user_id = user.id
        db.session.delete(user)
        db.session.commit()
        deleted = User.query.filter_by(id=user_id).first()
        self.assertTrue( deleted is None)




#email address and passwords used for unit testing
users=[]

users.append(['testuser1', 'testuser1@hotmail.com', 1, 'Alice', 'Doe', 'test']) #admin
users.append(['testuser2', 'testuser2@hotmail.com', 0, 'Mary', 'Doe', 'test'])  #user
users.append(['testuser3', 'testuser3@hotmail.com', 0, 'John', 'Doe', 'test'])  #user

#create testuser1 and testuser2 accounts if they do not exist

for i in range(0, len(users) - 1):
    user = User.query.filter_by(nickname=users[i][0]).first()
    if user is None:
            user = User(nickname=users[i][0], email=users[i][1], role=users[i][2], first_name=users[i][3], last_name=users[i][4], password=users[i][5], date_registered=datetime.utcnow())
            db.session.add(user)
            db.session.commit()

#make testuser1 an admin
user = User.query.filter_by(email=users[0][1]).first()
user.role = 1
db.session.commit()

#delete testuser3 account if it exists
user = User.query.filter_by(nickname=users[2][0]).first()
if user is not None:
        db.session.delete(user)
        db.session.commit()

class TestCaseRegistration(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' +os.path.join(basedir,'app.db')
        self.app = app.test_client()

    def tearDown(self):
        pass

    def test_registration_success(self):

        rv=self.app.post('/register', data=dict(first_name=users[2][3], last_name=users[2][4], email=users[2][1], nickname=users[2][0], password=users[2][5], confirm_password=users[2][5]), follow_redirects=True)
        self.assertTrue( 'Congratulations!  Your registration was successful!' in rv.data )

    def test_registration_email_exists(self):
        rv=self.app.post('/register', data=dict(first_name='Isatou', last_name='Sanneh', email=users[0][1], nickname='testisanneh', password='test', confirm_password='test'), follow_redirects=True)
        self.assertTrue( 'Email Address already exists!' in rv.data )

    def test_registration_nickname_exists(self):
        rv=self.app.post('/register', data=dict(first_name='Isatou', last_name='Sanneh', email='isatou@example.com', nickname=users[0][0], password='test', confirm_password='test'), follow_redirects=True)
        self.assertTrue( 'Nickname is already taken. Please choose another nickname!' in rv.data )

    def test_registration_passwords_different(self):
        rv=self.app.post('/register', data=dict(first_name='Isatou', last_name='Sanneh', email='isatou@example.com', nickname='isanneh', password='test', confirm_password='test2'), follow_redirects=True)
        self.assertTrue( 'Passwords must match' in rv.data )

class TestCaseLogin2Logout(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'app.db')
        self.app = app.test_client()

    def tearDown(self):
        pass

    def test_signin_login_success(self):
        rv = self.app.post('/signin', data=dict(email=users[0][1], password=users[0][5]), follow_redirects=True)
        self.assertTrue( 'You have been successfully logged in as' in rv.data )
        rv = self.app.get('/logout', follow_redirects=True)
        self.assertTrue( 'You have been successfully logged out!' in rv.data )

    def test_signin_invalid_email(self):
        rv = self.app.post('/signin', data=dict(email='wrong@example.com', password='test'), follow_redirects=True)
        self.assertTrue( 'Email Address and/or password is invalid!' in rv.data )

    def test_signin_invalid_password(self):
        rv = self.app.post('/signin', data=dict(email='sam@example.com', password='wrong'), follow_redirects=True)
        self.assertTrue( 'Email Address and/or password is invalid!' in rv.data )

class TestCaseAddAdmin(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'app.db')
        self.app = app.test_client()

        self.app.post('/signin', data=dict(email=users[0][1], password=users[0][5]), follow_redirects=True)

    def tearDown(self):
        self.app.get('/logout', follow_redirects=True)

    def test_add_admin_success(self):
        rv = self.app.post('/add_admin', data=dict(email=users[1][1], confirm_email=users[1][1]), follow_redirects=True)
        self.assertTrue( 'has been appointed as an admin!' in rv.data )

        user = User.query.filter_by(email=users[1][1]).first()
        user.role = 0
        db.session.commit()

    def test_add_admin_emails_different(self):
        rv=self.app.post('/add_admin', data=dict(email=users[1][1], confirm_email='wrong@example.com'), follow_redirects=True)
        self.assertTrue( 'Emails must match' in rv.data )

    def test_add_admin_non_existent_email(self):
        rv=self.app.post('/add_admin', data=dict(email='wrong@example.com', confirm_email='wrong@example.com'), follow_redirects=True)
        self.assertTrue( 'The email address owner is not a member of Smart House' in rv.data )

class TestCaseAdminManageUsers(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'app.db')
        self.app = app.test_client()

        self.app.post('/signin', data=dict(email=users[0][1], password=users[0][5]), follow_redirects=True)

    def tearDown(self):
        self.app.get('/logout', follow_redirects=True)

    def test_disable_account(self):
        user=User.query.filter_by(nickname=users[1][0]).first()
        path='/admin/disable_account/' + str(user.id)
        rv=self.app.get(path, follow_redirects=True)
        self.assertTrue( 'account has been disabled!' in rv.data )

    def test_enable_account(self):
        user = User.query.filter_by(nickname=users[1][0]).first()
        path = '/admin/enable_account/' + str(user.id)
        rv = self.app.get(path, follow_redirects=True)
        self.assertTrue( 'account has been enabled!' in rv.data )

class TestCaseBanUser(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['CSRF_ENABLED'] = False
        self.app = app.test_client()

        rv=self.app.post('/signin', data=dict(email=users[0][1], password=users[0][5]), follow_redirects=True)

    def tearDown(self):
        rv = self.app.get('/logout', follow_redirects=True)

    def test_prevent_user_login(self):

        #disable user account
        user = User.query.filter_by(nickname=users[1][0]).first()
        path = '/admin/disable_account/' + str(user.id)
        rv = self.app.get(path, follow_redirects=True)
        rv = self.app.get('/logout', follow_redirects=True)
        #log in disabled account
        rv = self.app.post('/signin', data=dict(email=users[1][1], password=users[1][5]), follow_redirects=True)
        self.assertTrue( 'Sorry, you cannot log in because your account was disabled by an admin!' in rv.data )

    def test_login_re_enabled_account(self):

        #enable user account that was disabled
        user = User.query.filter_by(nickname=users[1][0]).first()
        path = '/admin/enable_account/' + str(user.id)
        self.app.get(path, follow_redirects=True)
        self.app.get('/logout', follow_redirects=True)

        #log in re-enabled account to see if it'll work
        rv = self.app.post('/signin', data=dict(email=users[1][1], password=users[1][5]), follow_redirects=True)
        self.assertTrue( 'You have been successfully logged in as' in rv.data )


class TestCaseAccessDenied(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['CSRF_ENABLED'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'app.db')
        self.app = app.test_client()
        self.app.post('/signin', data=dict(email=users[1][1], password=users[1][5]), follow_redirects=True)

    def tearDown(self):
        self.app.get('/logout', follow_redirects=True)

    def test_access_denied(self):
        rv = self.app.post('/add_admin', follow_redirects=True)
        self.assertTrue( 'Sorry, you do not have access to this page!' in rv.data )

        rv = self.app.get('/manage_users', follow_redirects=True)
        self.assertTrue( 'Sorry, you do not have access to this page!' in rv.data )

        rv = self.app.get('/admins', follow_redirects=True)
        self.assertTrue( 'Sorry, you do not have access to this page!' in rv.data )


if __name__ == '__main__':
    unittest.main()
