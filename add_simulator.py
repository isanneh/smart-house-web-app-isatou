#!shwa_env/bin/python
from app import db, models
from datetime import datetime

def add_admin(email_address, password):
    user = models.User.query.filter_by(email=email_address, 
           password=password).first()
    if user is None:
        u = models.User(email=email_address, nickname='Simulator', 
            password=password, first_name='simulator', last_name='simulator', 
            date_registered=datetime.utcnow(), role = models.ROLE_ADMIN)
        db.session.add(u)
        print "Simulator account successfully added."
        #Add simulator devices
        print "Populating simulator..."
        wc_us = ["http://81.134.25.182/anony/mjpg.cgi", 
            "http://64.150.193.71/mjpg/video.mjpg",
            "http://187.162.141.27:8080/anony/mjpg.cgi",
            "http://203.207.50.119:60002/nphMotionJpeg?Resolution=640x480&Quality=Clarity",
            "http://200.79.225.92:8080/mjpg/video.mjpg"]
        for j in range(100, 106):
            r = models.Room(name='room-w-ip-ending-in-'+str(j), user=u, webcam_url=wc_us[j%len(wc_us)])
            db.session.add(r)
            print "Adding room " + r.name
            for i in range(1, 4):
                d = models.Device(user=u, room= r, name='device-on-relay'+str(i), 
                url='http://192.168.1.'+str(j)+':334/relay'+str(i), 
                command='on', command_optional='off', request_type='GET', 
                last_used=datetime.utcnow(), message = 'Created')
                db.session.add(d)
                print 'Adding device '+d.name+' to room '+d.room.name
        db.session.commit()
        print "...done."
    else:
        print "Simulator account already exists."

if __name__ == '__main__':
    #email_address = sys.argv[1]
    #password = sys.argv[2]
    add_admin('simulator@smarthouse.com', 'simulator')

