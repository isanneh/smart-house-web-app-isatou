from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest, time, re

class ForumTestRedo(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://shwa.herokuapp.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_forum_test_redo(self):
        driver = self.driver
        driver.get(self.base_url + "/signin")
        driver.find_element_by_link_text("Smart House Web App").click()
        driver.find_element_by_link_text("Register").click()
        driver.find_element_by_id("first_name").clear()
        driver.find_element_by_id("first_name").send_keys("forum")
        driver.find_element_by_id("last_name").clear()
        driver.find_element_by_id("last_name").send_keys("one")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("f1@gmail.com")
        driver.find_element_by_id("nickname").clear()
        driver.find_element_by_id("nickname").send_keys("forum1")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_id("confirm_password").clear()
        driver.find_element_by_id("confirm_password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Register").click()
        driver.find_element_by_id("first_name").clear()
        driver.find_element_by_id("first_name").send_keys("forum")
        driver.find_element_by_id("last_name").clear()
        driver.find_element_by_id("last_name").send_keys("two")
        driver.find_element_by_id("nickname").clear()
        driver.find_element_by_id("nickname").send_keys("forum2")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_id("confirm_password").clear()
        driver.find_element_by_id("confirm_password").send_keys("test")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("f2@gmail.com")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("f1@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Forum").click()
        driver.find_element_by_link_text("Create new post").click()
        driver.find_element_by_id("title").clear()
        driver.find_element_by_id("title").send_keys("forum test")
        driver.find_element_by_id("body").clear()
        driver.find_element_by_id("body").send_keys("this is a test for forum functionality")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Sign out").click()
        driver.find_element_by_css_selector("a.btn.btn-small").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("f2@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Forum").click()
        driver.find_element_by_link_text("forum test").click()
        # Warning: verifyTextPresent may require manual changes
        try: self.assertRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*this is a test for forum functionality[\s\S]*$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Reply Post").click()
        driver.find_element_by_id("body").clear()
        driver.find_element_by_id("body").send_keys("I can see the post")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Sign out").click()
        driver.find_element_by_css_selector("a.btn.btn-small").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("f1@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Forum").click()
        driver.find_element_by_link_text("forum test").click()
        # Warning: verifyTextPresent may require manual changes
        try: self.assertRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*I can see the post[\s\S]*$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Manage Account").click()
        driver.find_element_by_link_text("Delete Account").click()
        driver.find_element_by_css_selector("a.btn.btn-small").click()
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("f2@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Manage Account").click()
        driver.find_element_by_link_text("Delete Account").click()
	driver.get(self.base_url + "/register_mike")
	driver.find_element_by_link_text("Sign in").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("m@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("tes")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Forum").click()
        driver.find_element_by_link_text("forum test").click()
        driver.find_element_by_link_text("Delete Post").click()
        driver.find_element_by_link_text("Sign out").click()
    
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
