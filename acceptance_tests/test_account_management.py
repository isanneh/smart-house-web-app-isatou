from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest, time, re

class TestAccountManagementRedo(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://shwa.herokuapp.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_account_management_redo(self):
        driver = self.driver
        driver.get(self.base_url + "/home")
        driver.find_element_by_xpath("(//a[contains(text(),'Register')])[2]").click()
        driver.find_element_by_id("first_name").clear()
        driver.find_element_by_id("first_name").send_keys("acc")
        driver.find_element_by_id("last_name").clear()
        driver.find_element_by_id("last_name").send_keys("test")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("acctest@test.com")
        driver.find_element_by_id("nickname").clear()
        driver.find_element_by_id("nickname").send_keys("tester1")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_id("confirm_password").clear()
        driver.find_element_by_id("confirm_password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("acctest@test.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        # Warning: verifyTextPresent may require manual changes
        try: self.assertRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*Welcome, tester1![\s\S]*$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Manage Account").click()
        driver.find_element_by_id("nickname").clear()
        driver.find_element_by_id("nickname").send_keys("tester")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("acctest@gmail.com")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Manage Account").click()
        driver.find_element_by_link_text("Change your password").click()
        driver.find_element_by_id("old_password").clear()
        driver.find_element_by_id("old_password").send_keys("test")
        driver.find_element_by_id("new_password").clear()
        driver.find_element_by_id("new_password").send_keys("tess")
        driver.find_element_by_id("confirm_password").clear()
        driver.find_element_by_id("confirm_password").send_keys("tess")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Sign out").click()
        driver.find_element_by_link_text("Sign in").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("acctest@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("tess")
        driver.find_element_by_css_selector("input.btn").click()
        # Warning: verifyTextPresent may require manual changes
        try: self.assertRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*Welcome, tester![\s\S]*$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Manage Account").click()
        driver.find_element_by_link_text("Delete Account").click()
        driver.find_element_by_link_text("Sign in").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("acctest@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("tess")
        driver.find_element_by_css_selector("input.btn").click()
        # Warning: verifyTextPresent may require manual changes
        try: self.assertRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*Email Address and/or password is invalid![\s\S]*$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("acctest@test.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        # Warning: verifyTextPresent may require manual changes
        try: self.assertRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*Email Address and/or password is invalid![\s\S]*$")
        except AssertionError as e: self.verificationErrors.append(str(e))
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
