from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest, time, re

class TimedForum1(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://shwa.herokuapp.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_timed_forum1(self):
        driver = self.driver
        driver.get(self.base_url + "/home")
        driver.find_element_by_link_text("Register").click()
        driver.find_element_by_id("first_name").clear()
        driver.find_element_by_id("first_name").send_keys("f1")
        driver.find_element_by_id("last_name").clear()
        driver.find_element_by_id("last_name").send_keys("f2")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("forum@gmail.com")
        driver.find_element_by_id("nickname").clear()
        driver.find_element_by_id("nickname").send_keys("f1")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_id("confirm_password").clear()
        driver.find_element_by_id("confirm_password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("forum@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Forum").click()
        driver.find_element_by_link_text("Create new post").click()
        driver.find_element_by_id("title").clear()
        driver.find_element_by_id("title").send_keys("forum test 12456789")
        driver.find_element_by_id("body").clear()
        driver.find_element_by_id("body").send_keys("test 1234")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Manage Account").click()
        driver.find_element_by_link_text("Delete Account").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
