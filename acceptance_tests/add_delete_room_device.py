from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest, time, re

class AddDeleteRoomDevice(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://shwa.herokuapp.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_add_delete_room_device(self):
        driver = self.driver
        driver.get(self.base_url + "/home")
        driver.find_element_by_css_selector("a.btn.btn-small").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("m@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("tes")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_css_selector("a[title=\"view your smart house\"] > img[alt=\"my house\"]").click()
        driver.find_element_by_link_text("Rooms").click()
        driver.find_element_by_link_text("Add room").click()
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys("test_room")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Add device").click()
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys("test_device")
        driver.find_element_by_id("url").clear()
        driver.find_element_by_id("url").send_keys("http://test.com/test")
        driver.find_element_by_id("command").clear()
        driver.find_element_by_id("command").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Sign out").click()
        driver.find_element_by_css_selector("a.btn.btn-small").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("m@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("tes")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Rooms").click()
        # Warning: verifyTextPresent may require manual changes
        try: self.assertRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*test_room[\s\S]*$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        # Warning: verifyTextPresent may require manual changes
        try: self.assertRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*test_device[\s\S]*$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("delete").click()
        driver.find_element_by_link_text("Delete room").click()
        driver.find_element_by_link_text("Sign out").click()
        driver.find_element_by_css_selector("a.btn.btn-small").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("m@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("tes")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Rooms").click()
        # Warning: verifyTextNotPresent may require manual changes
        try: self.assertNotRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*test_room[\s\S]*$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        # Warning: verifyTextNotPresent may require manual changes
        try: self.assertNotRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*test_device[\s\S]*$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Sign out").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
