from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest, time, re

class RegisterExisitingEmail(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://shwa.herokuapp.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_register_exisiting_email(self):
        driver = self.driver
        driver.get(self.base_url + "/register")
        driver.find_element_by_id("first_name").clear()
        driver.find_element_by_id("first_name").send_keys("m")
        driver.find_element_by_id("last_name").clear()
        driver.find_element_by_id("last_name").send_keys("s")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("m@gmail.com")
        driver.find_element_by_id("nickname").clear()
        driver.find_element_by_id("nickname").send_keys("ms")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("tes")
        driver.find_element_by_id("confirm_password").clear()
        driver.find_element_by_id("confirm_password").send_keys("tes")
        driver.find_element_by_css_selector("input[type=\"submit\"]").click()
        # Warning: verifyTextPresent may require manual changes
        try: self.assertRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*Email Address already exists![\s\S]*$")
        except AssertionError as e: self.verificationErrors.append(str(e))
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert.text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
