#!shwa_env/bin/python
from app import db, models
from datetime import datetime

def add_admin(email_address, password):
    user = models.User.query.filter_by(email=email_address, 
           password=password).first()
    if user is None:
        u = models.User(email = email_address, nickname = 'Demo', 
            password = password, first_name = 'Demo', last_name = 'Demo', 
            date_registered = datetime.utcnow())
        db.session.add(u)
        print "Demo account successfully added."
        #Add simulator devices
        print "Populating demo..."
        r = models.Room(name = 'DemoRoom', user = u, webcam_url="http://72.225.242.169:8353/")
        db.session.add(r)
        print "Adding room " + r.name
        device_list = ['XmassTree', 'fan', 'lamp']
        for i in range(1, 4):
            d = models.Device(user = u, room = r, name = device_list[i-1],
            url='http://72.225.242.169:334/relay'+str(i),
            command='on', command_optional='off', request_type='GET',
            last_used=datetime.utcnow(), message = 'Created')
            db.session.add(d)
            print 'Adding device '+d.name+' to room '+d.room.name
        r = models.Room(name = 'DemoRoomLocal', user = u, webcam_url="http://64.150.193.71/mjpg/video.mjpg")
        db.session.add(r)
        print "Adding room " + r.name
        device_list = ['fan', 'lamp']
        for i in range(1, 3):
            d = models.Device(user = u, room = r, name = device_list[i-1],
            url='http://192.168.0.100/relay'+str(i),
            command='on', command_optional='off', request_type='GET',
            last_used=datetime.utcnow(), message = 'Created')
            db.session.add(d)
            print 'Adding device '+d.name+' to room '+d.room.name
        db.session.commit()
        print "...done."
    else:
        print "Demo account already exists."

if __name__ == '__main__':
    #email_address = sys.argv[1]
    #password = sys.argv[2]
    add_admin('demo@smarthouse.com', 'd3m0nstr4t10n')