#!shwa_env/bin/python
import sys
from app import db, models

def add_admin(admin_email, admin_pass):
    user = models.User.query.filter_by(email=admin_email, 
           password=admin_pass).first()
    
    if user is not None:
        user.role = models.ROLE_ADMIN
        db.session.commit()
        print "Admin successfully added"
    else:
        print """Admin couldn't be added.  
        Incorrect email address and/or password"""

if __name__ == '__main__':
    email_address = sys.argv[1]
    password = sys.argv[2]
    add_admin(email_address, password)